import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {AngularMaterialModule} from "./AngularMaterialModule";
import {RpgcharacteriaBackService} from "./services/rpgcharacteria-back.service";
import {
  RpgcharacteriaBackListComponent
} from "./components/rpgcharacteria-back-list/rpgcharacteria-back-list.component";
import {
  RpgcharacteriaBackSaveComponent
} from "./components/rpgcharacteria-back-save/rpgcharacteria-back-save.component";

@NgModule({
  declarations: [
    AppComponent,
    RpgcharacteriaBackListComponent,
    RpgcharacteriaBackSaveComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AngularMaterialModule,
  ],
  providers: [RpgcharacteriaBackService],
  bootstrap: [AppComponent]
})
export class AppModule { }
