import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { BookDto } from '../dto/BookDto';
import { environment } from 'src/environments/environment.development';

@Injectable({
  providedIn: 'root'
})
export class RpgcharacteriaBackService {
  private apiUrl = `${environment.apiURL}/api/characters`;


  constructor(private http: HttpClient) {}

  findAllBooks(): Observable<BookDto[]> {
    return this.http.get<BookDto[]>(this.apiUrl);
  }

  saveBook(bookDtos: BookDto[]): Observable<BookDto[]> {
    return this.http.post<BookDto[]>(this.apiUrl, bookDtos);
  }
}
