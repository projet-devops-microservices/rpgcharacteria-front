import { TestBed } from '@angular/core/testing';

import { RpgcharacteriaBackService } from './rpgcharacteria-back.service';

describe('RpgcharacteriaBackService', () => {
  let service: RpgcharacteriaBackService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RpgcharacteriaBackService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
