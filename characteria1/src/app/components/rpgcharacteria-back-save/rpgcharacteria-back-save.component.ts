import { Component ,OnInit} from '@angular/core';
import {BookDto} from "../../dto/BookDto";
import {RpgcharacteriaBackService} from "../../services/rpgcharacteria-back.service";

@Component({
  selector: 'app-rpgcharacteria-back-save',
  templateUrl: './rpgcharacteria-back-save.component.html',
  styleUrls: ['./rpgcharacteria-back-save.component.css']
})
export class RpgcharacteriaBackSaveComponent{

  selectedBooks: BookDto[] = [];
  books: BookDto[] = [];

  constructor(private rpgcharacteriaBackService: RpgcharacteriaBackService) {}

  /*
  ngOnInit() {
    this.loadAllBooks();
  }

  loadAllBooks() {
    this.rpgcharacteriaBackService.findAllBooks().subscribe(books => {
      this.books = books;
    });
  }
*/
  selectBook(book: BookDto, index: number): void {
    const selectedIndex = this.selectedBooks.indexOf(book);
    console.log(`Le livre à l'index ${index} a été sélectionné`);
    if (selectedIndex > -1) {
      this.selectedBooks.splice(selectedIndex, 1);
    } else {
      this.selectedBooks.push(book);
    }
  }

  saveBooks(){
    this.rpgcharacteriaBackService.saveBook(this.selectedBooks).subscribe({
      next: (response) => {
        console.log("Les livres ont été enregistrés", response);
      },
      error: (error) => {
        console.error("Erreur lors de l'enregistrement des livres", error);
      }
    });
  }

}
