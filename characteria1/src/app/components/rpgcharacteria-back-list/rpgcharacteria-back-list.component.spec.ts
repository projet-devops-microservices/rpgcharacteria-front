import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpgcharacteriaBackListComponent } from './rpgcharacteria-back-list.component';

describe('RpgcharacteriaBackListComponent', () => {
  let component: RpgcharacteriaBackListComponent;
  let fixture: ComponentFixture<RpgcharacteriaBackListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RpgcharacteriaBackListComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RpgcharacteriaBackListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
