import { Component, OnInit } from '@angular/core';
import { BookDto } from '../../dto/BookDto';
import { RpgcharacteriaBackService } from '../../services/rpgcharacteria-back.service';

@Component({
  selector: 'app-rpgcharacteria-back-list',
  templateUrl: './rpgcharacteria-back-list.component.html',
  styleUrls: ['./rpgcharacteria-back-list.component.css']
})
export class RpgcharacteriaBackListComponent {
  books: BookDto[] = [];

  constructor(private rpgcharacteriaBackService: RpgcharacteriaBackService) {}

  ngOnInit() {
    this.loadAllBooks();
  }

  loadAllBooks() {
    this.rpgcharacteriaBackService.findAllBooks().subscribe(books => {
      this.books = books;
    });
  }

}
