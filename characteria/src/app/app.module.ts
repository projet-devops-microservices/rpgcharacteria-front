import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { RpgcharacteriaInterfaceComponent } from "./rpgcharacteria-interface/rpgcharacteria-interface.component";
import { RpgcharacteriaImgComponent } from "./rpgcharacteria-img/rpgcharacteria-img.component";
import { RpgcharacteriaHistoryComponent } from "./rpgcharacteria-history/rpgcharacteria-history.component";
import { RpgcharacteriaBackSaveComponent } from "./rpgcharacteria-back-save/rpgcharacteria-back-save.component";
import { RpgcharacteriaBackListComponent } from "./rpgcharacteria-back-list/rpgcharacteria-back-list.component";
import { DisplayComponent } from "./display/display.component";
import { AngularMaterialModule } from "./AngularMaterialModule";
import { routes } from "./app.routes";

@NgModule({
  declarations: [
    AppComponent,
    RpgcharacteriaInterfaceComponent,
    RpgcharacteriaImgComponent,
    RpgcharacteriaHistoryComponent,
    RpgcharacteriaBackSaveComponent,
    RpgcharacteriaBackListComponent,
    DisplayComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AngularMaterialModule,
    RouterModule.forRoot(routes),
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
