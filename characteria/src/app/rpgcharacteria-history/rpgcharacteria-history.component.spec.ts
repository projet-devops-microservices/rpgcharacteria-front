import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpgcharacteriaHistoryComponent } from './rpgcharacteria-history.component';

describe('RpgcharacteriaHistoryComponent', () => {
  let component: RpgcharacteriaHistoryComponent;
  let fixture: ComponentFixture<RpgcharacteriaHistoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RpgcharacteriaHistoryComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RpgcharacteriaHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
