import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpgcharacteriaImgComponent } from './rpgcharacteria-img.component';

describe('RpgcharacteriaImgComponent', () => {
  let component: RpgcharacteriaImgComponent;
  let fixture: ComponentFixture<RpgcharacteriaImgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RpgcharacteriaImgComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RpgcharacteriaImgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
