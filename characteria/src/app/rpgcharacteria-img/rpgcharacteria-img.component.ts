import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-rpgcharacteria-img',

  templateUrl: './rpgcharacteria-img.component.html',
  styleUrls: ['./rpgcharacteria-img.component.css']
})
export class RpgcharacteriaImgComponent {
  @Input() imageUrl: string | undefined;

}
