import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpgcharacteriaInterfaceComponent } from './rpgcharacteria-interface.component';

describe('RpgcharacteriaInterfaceComponent', () => {
  let component: RpgcharacteriaInterfaceComponent;
  let fixture: ComponentFixture<RpgcharacteriaInterfaceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RpgcharacteriaInterfaceComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RpgcharacteriaInterfaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
