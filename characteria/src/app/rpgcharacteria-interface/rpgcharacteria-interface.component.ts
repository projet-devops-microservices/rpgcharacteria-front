import {Component} from '@angular/core';
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {RpgcharacteriaBackListComponent} from "../rpgcharacteria-back-list/rpgcharacteria-back-list.component";


interface Classe {
  value: string;
  viewValue: string;
}
interface Specie {
  value: string;
  viewValue: string;
}
interface Job {
  value: string;
  viewValue: string;
}



/** @title Select with 2-way value binding */
@Component({
  selector: 'app-rpgcharacteria-interface',
  templateUrl: './rpgcharacteria-interface.component.html',
  styleUrls: ['./rpgcharacteria-interface.component.css'],

})
export class RpgcharacteriaInterfaceComponent {
  selectedImage: string | undefined;
  selectedDescription: string | undefined;

  Classes: Classe[] = [
    {value: 'Cyberninja-0', viewValue: 'Cyberninja'},
    {value: 'Extraterrestria-1', viewValue: 'Extraterrestria'},
    {value: 'The Elder Scrolls-2', viewValue: 'The Elder Scrolls'},
  ];
  Species: Specie[] = [
    {value: 'Cosmic Cyborg-0', viewValue: 'Cosmic Cyborg'},
    {value: 'Stellar Emissary-1', viewValue: 'Stellar Emissary'},
    {value: 'Rune Guardian-2', viewValue: 'Rune Guardian'},
  ];
  Jobs: Job[] = [
    {value: 'Master of Quantum Electronics-0', viewValue: 'Master of Quantum Electronics'},
    {value: 'Intergalactic Diplomat-1', viewValue: 'Intergalactic Diplomat'},
    {value: 'Archivist of The Elder Scrolls-2', viewValue: 'Archivist of The Elder Scrolls'},
  ];

  generate() {
    // Placeholder logic for generating image and description
    this.selectedImage = 'path_to_image.jpg';
    this.selectedDescription = 'Description goes here';
  }
}
