import { Component } from '@angular/core';
import {RpgcharacteriaBackSaveComponent} from "../rpgcharacteria-back-save/rpgcharacteria-back-save.component";
import {RpgcharacteriaHistoryComponent} from "../rpgcharacteria-history/rpgcharacteria-history.component";
import {RpgcharacteriaImgComponent} from "../rpgcharacteria-img/rpgcharacteria-img.component";
import {RpgcharacteriaInterfaceComponent} from "../rpgcharacteria-interface/rpgcharacteria-interface.component";

@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent {

}
