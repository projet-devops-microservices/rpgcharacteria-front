import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RpgcharacteriaBackSaveComponent } from './rpgcharacteria-back-save.component';

describe('RpgcharacteriaBackSaveComponent', () => {
  let component: RpgcharacteriaBackSaveComponent;
  let fixture: ComponentFixture<RpgcharacteriaBackSaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RpgcharacteriaBackSaveComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RpgcharacteriaBackSaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
