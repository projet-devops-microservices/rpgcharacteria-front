import { TestBed } from '@angular/core/testing';

import { RpgcharacteriaImgService } from './rpgcharacteria-img.service';

describe('RpgcharacteriaImgService', () => {
  let service: RpgcharacteriaImgService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RpgcharacteriaImgService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
