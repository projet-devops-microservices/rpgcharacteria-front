import { TestBed } from '@angular/core/testing';

import { RpgcharacteriaHistoryService } from './rpgcharacteria-history.service';

describe('RpgcharacteriaHistoryService', () => {
  let service: RpgcharacteriaHistoryService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RpgcharacteriaHistoryService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
