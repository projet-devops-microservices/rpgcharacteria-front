import { Component, OnInit } from '@angular/core';
import { BookDto } from '../dto/BookDto';
import { BookService } from '../services/rpgcharacteria-back.service';


@Component({
  selector: 'app-rpgcharacteria-back-list',

  templateUrl: './rpgcharacteria-back-list.component.html',
  styleUrls: ['./rpgcharacteria-back-list.component.css']
})
export class RpgcharacteriaBackListComponent {
  books: BookDto[] = [];

  constructor(private bookService: BookService) {}

  ngOnInit() {
    this.loadAllBooks();
  }

  loadAllBooks() {
    this.bookService.findAllBooks().subscribe(books => {
      this.books = books;
    });
  }

}
